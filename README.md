# README #

This is a web service for users management in the system.

### What are currently available endpoints ###

* Create new user: /api/register
* Edit user:       /api/editProfile

In future releases there will be these endpoints:
* list all users (with filters and pagination)
* change password
* edit user by admin